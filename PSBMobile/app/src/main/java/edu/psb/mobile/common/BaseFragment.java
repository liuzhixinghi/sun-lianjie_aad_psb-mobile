package edu.psb.mobile.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;

public abstract class BaseFragment extends Fragment {
    /**
     * Fragment当前状态是否可见
     */
    protected boolean isVisible;
    /**
     * Fragment当前状态是否创建
     */
    protected Boolean isCreated = false;

    public FragmentActivity mAcctivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mAcctivity = (FragmentActivity) context;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        isCreated = true;
        if (isVisible) {
            onVisible();
        }
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            isVisible = true;
            if (isCreated) {
                onVisible();
            }
        } else {
            isVisible = false;
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    /**
     * 可见
     */
    protected void onVisible() {
        lazyLoad();
    }

    /**
     * 延迟加载
     */
    protected abstract void lazyLoad();

}
