package edu.psb.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.psb.mobile.R;
import edu.psb.mobile.util.SharePreferenceMgr;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ll_people)
    LinearLayout llPeople;
    @BindView(R.id.ll_booking)
    LinearLayout llBooking;
    @BindView(R.id.ll_past)
    LinearLayout llPast;
    @BindView(R.id.ll_helps)
    LinearLayout llHelps;
    @BindView(R.id.ll_helps_request)
    LinearLayout llHelpsRequest;
    @BindView(R.id.ll_pass)
    LinearLayout llPass;
    @BindView(R.id.ll_faq)
    LinearLayout llFaq;
    @BindView(R.id.ll_logout)
    LinearLayout llLogout;
    @BindView(R.id.tv_username)
    TextView tvUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        String name = (String) SharePreferenceMgr.get(this, "user_name", "Lachlan");
        tvUsername.setText(name);
    }

    @OnClick({R.id.ll_people, R.id.ll_booking, R.id.ll_past, R.id.ll_helps, R.id.ll_helps_request, R.id.ll_pass, R.id.ll_faq, R.id.ll_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_people:
                break;
            case R.id.ll_booking:
                Intent myBooking = new Intent(MainActivity.this, MyBookingActivity.class);
                startActivity(myBooking);
                break;
            case R.id.ll_past:
                Intent past = new Intent(MainActivity.this, PastBookingActivity.class);
                startActivity(past);
                break;
            case R.id.ll_helps:
                Intent i = new Intent(MainActivity.this, HelpsRegistrationActivity.class);
                startActivity(i);
                break;
            case R.id.ll_helps_request:
                break;
            case R.id.ll_pass:
                break;
            case R.id.ll_faq:
                break;
            case R.id.ll_logout:
                break;
        }
    }
}
