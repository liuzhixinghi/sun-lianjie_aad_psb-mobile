package edu.psb.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.psb.mobile.R;

public class ChooseActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.rg)
    RadioGroup rg;
    @BindView(R.id.bt_register)
    Button btRegister;
    @BindView(R.id.radioButton8)
    RadioButton radioButton8;
    @BindView(R.id.radioButton9)
    RadioButton radioButton9;
    @BindView(R.id.radioButton10)
    RadioButton radioButton10;
    @BindView(R.id.radioButton11)
    RadioButton radioButton11;
    @BindView(R.id.radioButton12)
    RadioButton radioButton12;
    @BindView(R.id.radioButton13)
    RadioButton radioButton13;
    @BindView(R.id.radioButton14)
    RadioButton radioButton14;
    @BindView(R.id.radioButton15)
    RadioButton radioButton15;
    @BindView(R.id.radioButton16)
    RadioButton radioButton16;
    @BindView(R.id.radioButton17)
    RadioButton radioButton17;
    @BindView(R.id.spinner)
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        ButterKnife.bind(this);
        rg.setOnCheckedChangeListener(this);
        setData(R.array.Coventry_University);
    }

    private void setData(int textArrayResId) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, textArrayResId,
                android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @OnClick(R.id.bt_register)
    public void onViewClicked() {
        Intent i = new Intent(ChooseActivity.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radioButton8:
                setData(R.array.Coventry_University);
                break;
            case R.id.radioButton9:
                setData(R.array.Edinburgh_Napier_University);
                break;
            case R.id.radioButton10:
                setData(R.array.Glasgow_Caledonian_University);
                break;
            case R.id.radioButton11:
                setData(R.array.La_Trobe_University);
                break;
            case R.id.radioButton12:
                setData(R.array.The_University_of_Hull);
                break;
            case R.id.radioButton13:
                setData(R.array.The_University_of_Newcastle_Australia);
                break;
            case R.id.radioButton14:
                setData(R.array.The_University_of_Nottingham);
                break;
            case R.id.radioButton15:
                setData(R.array.University_of_Wollongong);
                break;
            case R.id.radioButton16:
                setData(R.array.Cambridge_Assessment_International_Education);
                break;
        }
    }
}
