package edu.psb.mobile.activity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.psb.mobile.R;
import edu.psb.mobile.common.CommonRecyclerViewAdapter;
import edu.psb.mobile.domin.Course;
import io.realm.Realm;
import io.realm.RealmResults;

public class HelpsRegistrationActivity extends AppCompatActivity {

    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @BindView(R.id.ll_title)
    LinearLayout llTitle;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;

    private List<Course> coursesList;
    private CourseAdapter courseAdapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helps_registration);
        ButterKnife.bind(this);
        initData();
    }


    private void initData() {
        realm = Realm.getDefaultInstance();

        coursesList = new ArrayList<>();
        courseAdapter = new CourseAdapter(this, coursesList, R.layout.item_course);
        courseAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                final Course clickCourse = coursesList.get(position);
                if (clickCourse != null) {
                    if (clickCourse.isBooked()) {
                        clickCourse.setBooked(false);
                        clickCourse.setPast(false);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                clickCourse.deleteFromRealm();
                            }
                        });
                    } else {
                        clickCourse.setBooked(true);
                        clickCourse.setPast(false);
                        clickCourse.setId(generateNewPrimaryKey());
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(clickCourse);
                            }
                        });
                    }
                    courseAdapter.notifyItemChanged(position);
                }
            }
        });
        courseAdapter.setOnItemInfoClickListener(new CourseBookedAdapter.OnItemInfoClickListener() {
            @Override
            public void onItemInfoClick(int position) {
                Course clickCourse = coursesList.get(position);
                if (clickCourse != null) {
                    showDialogInfo(clickCourse.getInfo());
                }
            }
        });
        Resources res = getResources();
        String[] cources = res.getStringArray(R.array.Bachelor_of_Arts_with_Honours_in_Accounting_and_Finance);
        if (cources != null && cources.length > 0) {
            for (int i = 0; i < cources.length; i++) {
                Course course = new Course();
                course.setCourseName(cources[i]);
                course.setBooked(false);
                course.setLocation("CB01.05.10B");
                course.setTime("20th September 6:00 PM");
                course.setInfo(getString(R.string.info));
                coursesList.add(course);
            }
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(courseAdapter);
        recycleView.setHasFixedSize(true);
        courseAdapter.notifyDataSetChanged();
    }

    //获取最大的PrimaryKey并加一
    private long generateNewPrimaryKey() {
        long primaryKey = 0;
        RealmResults<Course> results = realm.where(Course.class).findAll();
        if (results != null && results.size() > 0) {
            Course last = results.last();
            primaryKey = last.getId() + 1;
        }
        return primaryKey;
    }

    @OnClick(R.id.img_menu)
    public void onViewClicked() {
        finish();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    private void showDialogInfo(String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("Course Info");
        builder.setMessage(info);//提示内容
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
