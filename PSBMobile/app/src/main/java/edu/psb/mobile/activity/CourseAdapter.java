package edu.psb.mobile.activity;

import android.content.Context;

import java.util.List;

import edu.psb.mobile.common.CommonRecyclerViewAdapter;
import edu.psb.mobile.common.ViewHolder;
import edu.psb.mobile.domin.Course;

public class CourseAdapter extends CommonRecyclerViewAdapter<Course> {


    public CourseAdapter(Context context, List<Course> data, int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public void convert(ViewHolder holder, Course item, OnItemClickListener mItemClickListener) {
        CourseViewHolder viewHolder = new CourseViewHolder(holder.itemView);
        viewHolder.showData(item, mItemClickListener, holder.getAdapterPosition(), mItemInfoClickListener);
    }

    public CourseBookedAdapter.OnItemInfoClickListener mItemInfoClickListener;

    public void setOnItemInfoClickListener(CourseBookedAdapter.OnItemInfoClickListener itemClickListener) {
        this.mItemInfoClickListener = itemClickListener;
    }

    public interface OnItemInfoClickListener {
        void onItemInfoClick(int position);
    }
}
