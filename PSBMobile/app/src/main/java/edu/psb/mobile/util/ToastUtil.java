package edu.psb.mobile.util;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class ToastUtil {
    // To display an error Toast:
    public static void error(Context context, String content) {
        Toasty.error(context, content, Toast.LENGTH_SHORT, true).show();
    }

    // To display a success Toast:
    public static void success(Context context, String content) {
        Toasty.success(context, content, Toast.LENGTH_SHORT, true).show();
    }

    // To display an info Toast:
    public static void info(Context context, String content) {
        Toasty.info(context, content, Toast.LENGTH_SHORT, true).show();
    }

    // To display a warning Toast:
    public static void warning(Context context, String content) {
        Toasty.warning(context, content, Toast.LENGTH_SHORT, true).show();
    }

    // To display the usual Toast:
    public static void normal(Context context, String content) {
        Toasty.normal(context, content).show();
    }
}
