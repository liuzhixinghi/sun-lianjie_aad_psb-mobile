package edu.psb.mobile.activity;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.psb.mobile.R;
import edu.psb.mobile.common.CommonRecyclerViewAdapter;
import edu.psb.mobile.common.ViewHolder;
import edu.psb.mobile.domin.Course;

public class CourseViewHolder extends ViewHolder {
    private TextView tv_name, tv_location, tv_time;
    private Button bt_book, bt_info;

    public CourseViewHolder(View itemView) {
        super(itemView);
        tv_name = getView(R.id.tv_name);
        tv_location = getView(R.id.tv_location);
        tv_time = getView(R.id.tv_time);
        bt_book = getView(R.id.bt_book);
        bt_info = getView(R.id.bt_info);
    }

    public void showData(Course course, final CommonRecyclerViewAdapter.OnItemClickListener mItemClickListener, final int position,
                         final CourseBookedAdapter.OnItemInfoClickListener itemInfoClickListener) {
        if (course != null) {
            tv_name.setText(course.getCourseName());
            tv_location.setText(course.getLocation());
            tv_time.setText(course.getTime());
            if (course.isBooked()) {
                bt_book.setText("BooKed");
                bt_book.setBackgroundResource(R.drawable.rect_dark_blue_bg);
            } else {
                bt_book.setText("BooK");
                bt_book.setBackgroundResource(R.drawable.rect_blue_bg);
            }
            bt_book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(position);
                    }
                }
            });

            bt_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemInfoClickListener != null) {
                        itemInfoClickListener.onItemInfoClick(position);
                    }
                }
            });
        }
    }
}
