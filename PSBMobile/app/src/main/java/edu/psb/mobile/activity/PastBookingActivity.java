package edu.psb.mobile.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.psb.mobile.R;
import edu.psb.mobile.common.CommonRecyclerViewAdapter;
import edu.psb.mobile.domin.Course;
import io.realm.Realm;
import io.realm.RealmResults;

public class PastBookingActivity extends AppCompatActivity {
    @BindView(R.id.img_menu)
    ImageView imgMenu;
    @BindView(R.id.ll_title)
    LinearLayout llTitle;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;

    private Realm realm;
    private List<Course> coursesList;
    private CourseBookedAdapter courseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();
        coursesList = new ArrayList<>();
        courseAdapter = new CourseBookedAdapter(this, coursesList, R.layout.item_course);
        courseAdapter.setOnItemClickListener(new CommonRecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position) {
                final Course clickCourse = coursesList.get(position);
                if (clickCourse != null) {
                    if (clickCourse.isBooked()) {
                        clickCourse.setBooked(false);
                        clickCourse.setPast(true);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(clickCourse);
                            }
                        });
                        /*final RealmResults<Course> courses = realm.where(Course.class).findAll();
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                if (courses != null && !courses.isEmpty()) {
                                    Course course = courses.get(position);
                                    course.deleteFromRealm();
                                }
                            }
                        });*/
                    } else {
                        clickCourse.setBooked(true);
                        clickCourse.setPast(false);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(clickCourse);
                            }
                        });
                    }
                    courseAdapter.notifyItemChanged(position);
                }
            }
        });
        courseAdapter.setOnItemInfoClickListener(new CourseBookedAdapter.OnItemInfoClickListener() {
            @Override
            public void onItemInfoClick(int position) {
                Course clickCourse = coursesList.get(position);
                if (clickCourse != null) {
                    showDialogInfo(clickCourse.getInfo());
                }
            }
        });
        obtainData();
    }

    private void showDialogInfo(String info) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle("Course Info");
        builder.setMessage(info);//提示内容
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void obtainData() {
        coursesList.clear();
        coursesList.addAll(queryAllHeartData());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(layoutManager);
        recycleView.setAdapter(courseAdapter);
        recycleView.setHasFixedSize(true);
        courseAdapter.notifyDataSetChanged();
    }

    public List<Course> queryAllHeartData() {
        if (realm != null) {
            RealmResults<Course> datas = realm.where(Course.class)
                    .equalTo("past", true)
                    .findAll();
            return realm.copyFromRealm(datas);
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @OnClick(R.id.img_menu)
    public void onViewClicked() {
        finish();
    }

    //获取最大的PrimaryKey并加一
    private long generateNewPrimaryKey() {
        long primaryKey = 0;
        RealmResults<Course> results = realm.where(Course.class).findAll();
        if(results != null && results.size() > 0) {
            Course last = results.last();
            primaryKey = last.getId() + 1;
        }
        return primaryKey;
    }
}
