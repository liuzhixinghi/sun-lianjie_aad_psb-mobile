package edu.psb.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import edu.psb.mobile.R;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.bt_next)
    Button btNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_next)
    public void onViewClicked() {
        Intent i = new Intent(RegistrationActivity.this, ChooseActivity.class);
        startActivity(i);
    }
}
