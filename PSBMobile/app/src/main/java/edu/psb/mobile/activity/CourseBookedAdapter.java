package edu.psb.mobile.activity;

import android.content.Context;

import java.util.List;

import edu.psb.mobile.common.CommonRecyclerViewAdapter;
import edu.psb.mobile.common.ViewHolder;
import edu.psb.mobile.domin.Course;

public class CourseBookedAdapter extends CommonRecyclerViewAdapter<Course> {


    public CourseBookedAdapter(Context context, List<Course> data, int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public void convert(ViewHolder holder, Course item, OnItemClickListener mItemClickListener) {
        CourseBookedViewHolder viewHolder = new CourseBookedViewHolder(holder.itemView);
        viewHolder.showData(item, mItemClickListener, holder.getAdapterPosition(), mItemInfoClickListener);
    }

    public OnItemInfoClickListener mItemInfoClickListener;

    public void setOnItemInfoClickListener(OnItemInfoClickListener itemClickListener) {
        this.mItemInfoClickListener = itemClickListener;
    }

    public interface OnItemInfoClickListener {
        void onItemInfoClick(int position);
    }
}
